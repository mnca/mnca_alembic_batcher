import sys,os
import maya.standalone
maya.standalone.initialize(name='python')
import maya.cmds as cmds
import maya.mel as mel

#opening file 
maya_file= sys.argv[1]
cmds.file(maya_file, o=True)

#READING SCENE INFO (all stored as string)
scene_info_object           = cmds.select('sceneInfo')
currentFileName             = cmds.file(q=True, sn=True, shn=True)[:-3] #without .ma
projectName                 = cmds.getAttr('sceneInfo.projName'     , asString=True)
episodeName                 = cmds.getAttr('sceneInfo.episodeName'  , asString=True)
projectCode                 = cmds.getAttr('sceneInfo.projCode'     , asString=True)
sequenceName                = cmds.getAttr('sceneInfo.sequenceName' , asString=True)
sceneInformation            = cmds.listAttr('sceneInfo'             , k=True)
frame_start                 = cmds.getAttr('sceneInfo.startFrame'   , asString=True)
frame_end                   = cmds.getAttr('sceneInfo.endFrame'     , asString=True)
current_scene_file_path     = cmds.file(query=True,sn=True) 

# reading all references (namespaces) , including duplicate one that storead as number
# only char and props that will be cached,set will be not_cached
#
ALL_REFERENCE=[]
imported_refs=cmds.ls('*:all')
for ref in imported_refs:
    #createing dict of each asset reference
    nameSpace           = ref.split(':')[0]
    unresolvedName      = cmds.referenceQuery(ref,filename=True,unresolvedName=True)
    sourceFile          = unresolvedName.split('{')[0]
    GEO_reference       = os.path.join(os.path.dirname(sourceFile),'GEO',os.path.basename(sourceFile))
    targetAlembicFile   = os.path.join("Y:",projectName,'ALEMBIC_CACHE',episodeName, sequenceName, nameSpace+".abc")
    #
    if nameSpace[:2] =='c_': assetType='char'
    if nameSpace[:2] =='p_': assetType='props'
    if nameSpace[:2] =='s_': assetType='sets'
    #
    ALL_REFERENCE.append({  'namespace'         : nameSpace ,
                            'unresolvedName'    : unresolvedName ,
                            'sourceFile'        : sourceFile,
                            'GEO_reference'     : GEO_reference,
                            'assetType'         : assetType,
                            'targetAlembicFile' : targetAlembicFile})

# exporting / saving scene Info and camera
cmds.select('sceneInfo')
sceneInfo_export_file = os.path.join("Y:",projectName,'ALEMBIC_CACHE',episodeName, sequenceName, "sceneInfo.ma").replace("\\",'/')
mel.eval('''file -force -options "v=0;" -typ "mayaAscii" -pr -es "{0}";'''.format(sceneInfo_export_file)  )
#
cmds.select('camMaster')
camera_export_file = os.path.join("Y:",projectName,'ALEMBIC_CACHE',episodeName, sequenceName, "camera.ma").replace("\\",'/')
mel.eval('''file -force -options "v=0;" -typ "mayaAscii" -pr -es "{0}";'''.format(camera_export_file)  )


# exporting to alembic
for asset in ALL_REFERENCE:
    print "Exporting " + asset['namespace'] +' to alembic ...'
    if asset['assetType'] == 'char' or  asset['assetType'] == 'props':
        #make sure createing alembic cache folder for export execution
        if not os.path.exists(os.path.dirname(asset['targetAlembicFile'])):
            os.makedirs(os.path.dirname(asset['targetAlembicFile']))

        asset_geo_root = asset['namespace'] + ":geo"
        fixed_abc_file = asset['targetAlembicFile'].replace('\\','/')
        mel_command='''AbcExport -j "-frameRange {0} {1} -noNormals -uvWrite -wholeFrameGeo -worldSpace -writeVisibility -dataFormat ogawa -root {2} -file {3} ";'''.format(frame_start,frame_end,asset_geo_root,fixed_abc_file)
        mel.eval(mel_command)


#create brand new file  =================================================================================================================================================================

cmds.file(new=True,f=True)
#createing  / renaming new name
new_alembic_shot = os.path.join(os.path.dirname(current_scene_file_path),"_render","shots", os.path.basename(current_scene_file_path).replace('.ma','_alembic.ma')  )   
cmds.file(rename=new_alembic_shot)

# reimport all assets
for asset in ALL_REFERENCE:
    cmds.file( asset['GEO_reference'] , r=True , namespace=asset['namespace'] )

# Importing camera and scene info
mel.eval('''file -import -type "mayaAscii"   "{0}";'''.format(sceneInfo_export_file) )
mel.eval('''file -import -type "mayaAscii"   "{0}";'''.format(camera_export_file) )


# connect / reimport alembic files char and props only
for asset in ALL_REFERENCE:
    print "Importing alembic file for " , asset['nameSpace']
    if asset['assetType'] == 'char' or  asset['assetType'] == 'props':
        asset_geo_root = asset['namespace'] + ":geo"
        mel_command='''AbcImport -mode import -connect "{0}" "{1}";'''.format(asset_geo_root,asset['targetAlembicFile'].replace('\\','/'))
        print mel_command
        mel.eval(mel_command)

#save file
cmds.file( save=True, type='mayaAscii',f=1 )

#closing and avoiding crash
maya.standalone.uninitialize()



#TODO: EXPORING CAMERA AND SCENE INFO