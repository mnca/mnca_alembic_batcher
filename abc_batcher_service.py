import subprocess,sqlite3,time,threading

class abc_batcher_service:
	
	DATABASE_LOCATION			=r"C:\work\repo\shot_batch"
	THREAD_SCAN_EVERY_SECOND	=3.0
	MAYA_PY_LOCATION			=r"C:\Program Files\Autodesk\Maya2018\bin\mayapy.exe"
	ABC_BATCHER_SCRIPT			=""

	def __init__(self):
		#createing main loop thread
		self.main_thread_process = threading.Thread(target = self.main_thread)
		self.main_thread_process.start()
		pass



	def main_thread(self):
		while True:
			queue_files=self.SQL_row_factory("SELECT FILE_PATH FROM QUEUE_TABLE")
			for maya_file in queue_files:
				process = subprocess.Popen([self.MAYA_PY_LOCATION,self.ABC_BATCHER_SCRIPT, maya_file['FILE_PATH']], stdout=subprocess.PIPE)
				dirtyOutput, err = process.communicate()
				
			time.sleep(self.THREAD_SCAN_EVERY_SECOND)

			
		
	def console_thread(self):
		while True:
			msg = raw_input('>')
			print msg
			
			
	def SQL_row_factory(self,command):
		#optimize for get data
		
		conn 	= sqlite3.connect(self.DATABASE_LOCATION)
		conn.row_factory = self.dict_factory
		cur = conn.cursor()
		cur.execute(command)
		
		return cur.fetchall() 
		
	def dict_factory(self,cursor, row):
		d = {}
		for idx, col in enumerate(cursor.description):
			d[col[0]] = row[idx]
		return d

	
		
	
app=abc_batcher_service()